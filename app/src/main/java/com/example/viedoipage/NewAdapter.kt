package com.example.viedoipage

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class NewAdapter: RecyclerView.Adapter<NewAdapter.VH>() {

    lateinit var list: List<Pair<String, String>>
    lateinit var context: Context

    fun setData(list: List<Pair<String, String>>, context: Context) {
        this.list = list
        this.context = context
    }

    class VH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name = itemView.findViewById<TextView>(R.id.name)
        val discr = itemView.findViewById<TextView>(R.id.discr)
        val back = itemView.findViewById<LinearLayout>(R.id.back)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return VH(LayoutInflater.from(parent.context).inflate(R.layout.item, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.name.text = list.get(position).first
        holder.discr.text = list.get(position).second
        holder.back.setOnClickListener {
            MainActivity.getClPos(position, context)
        }
    }
}