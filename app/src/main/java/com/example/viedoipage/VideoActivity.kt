package com.example.viedoipage

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import kotlinx.android.synthetic.main.videoitem.*

class VideoActivity : AppCompatActivity() {

    lateinit var player: SimpleExoPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)

        val intent = getIntent()

        player = SimpleExoPlayer.Builder(this)
            .build()

        val dataSource = DefaultDataSourceFactory(this)

        val mediaSource = ProgressiveMediaSource.Factory(dataSource)

        player.prepare(mediaSource.createMediaSource(Uri.parse("https://storage.googleapis.com/exoplayer-test-media-0/BigBuckBunny_320x180.mp4")))

        playerView.player = player
    }
}